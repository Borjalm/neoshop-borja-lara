/**
 * TODO: Desarrollar el código del slider
 */

var muestra = document.getElementById("sliders");
var sliders = new Array();
//cogida de datos del archivo json para realizar el slide
var slide = [{
    "id": 1,
    "title": "Título para la primera slide",
    "button_text": "Comprar",
    "button_link": "#",
    "bg_image": "resources/slider/slide1.jpg"
}, {
    "id": 2,
    "title": "Título para el segundo slide",
    "button_text": "Comprar",
    "button_link": "#",
    "bg_image": "resources/slider/slide2.jpg"
}, {
    "id": 3,
    "title": "Título para la tercera slide",
    "button_text": "Comprar",
    "button_link": "#",
    "bg_image": "resources/slider/slide3.jpg"
}, {
    "id": 4,
    "title": "Título para la cuarta slide",
    "button_text": "Comprar",
    "button_link": "#",
    "bg_image": "resources/slider/slide4.jpg"
}];

//Bucle que recorre el array json y guarda los parámetros en un array js
for (var x = 0; x < slide.length; x++) {
    sliders.push(`<img class='mySlides' src='${slide[x].bg_image}'></img>`);
    muestra.innerHTML += sliders[x];
}
//función cogida de internet que adapté para realizar el slide
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
    showDivs(slideIndex += n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    if (n > x.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = x.length
    }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[slideIndex - 1].style.display = "block";
}
/**
 * TODO: Desarrollar el código para cargar productos por Ajax

 */
//variable que usaremos para mostrar el catálogo
var muestraCatalogo = document.getElementById("catalogo");
//cogida de datos del archivo json para mostrar el catálogo
var productos = [{
        "id": 1,
        "name": "OLD SKOOL 36 DX",
        "price": "85,00 €",
        "button_text": "Comprar",
        "button_link": "#",
        "image": "resources/products/vans1.png"
    },
    {
        "id": 2,
        "name": "COMFYCUSH OLD SKOOL",
        "price": "75,00 €",
        "button_text": "Comprar",
        "button_link": "#",
        "image": "resources/products/vans2.png"
    },
    {
        "id": 3,
        "name": "OLD SKOOL DE ANTE Y LONA",
        "price": "60,00 €",
        "button_text": "Comprar",
        "button_link": "#",
        "image": "resources/products/vans3.png"
    },
    {
        "id": 4,
        "name": "OLD SKOOL DE ANTE SUAVE",
        "price": "65,00 €",
        "button_text": "Comprar",
        "button_link": "#",
        "image": "resources/products/vans4.png"
    },
    {
        "id": 5,
        "name": "OLD SKOOL PRO",
        "price": "95,00 €",
        "button_text": "Comprar",
        "button_link": "#",
        "image": "resources/products/vans5.png"
    },
    {
        "id": 6,
        "name": "ANTI HERO OLD SKOOL PRO",
        "price": "95,00 €",
        "button_text": "Comprar",
        "button_link": "#",
        "image": "resources/products/vans6.png"
    },
    {
        "id": 7,
        "name": "ERA PRIMARY CHECK",
        "price": "95,00 €",
        "button_text": "Comprar",
        "button_link": "#",
        "image": "resources/products/vans7.png"
    },
    {
        "id": 8,
        "name": "STYLE 36",
        "price": "80,00 €",
        "button_text": "Comprar",
        "button_link": "#",
        "image": "resources/products/vans8.png"
    }
];



//Bucle que recorre el array json y nos lo muestra
function mostrarCatalogo() {
    for (var y = 0; y <= productos.length + 2; y++) {
        //condicionales para las fotos que no pertenecen al json
        if (y == 4) {
            muestraCatalogo.innerHTML += "<div class='producto'style='width:185%;'><img src='resources/cta/cta1.jpg'></img></div></br>";
        } else if (y == 7) {
            muestraCatalogo.innerHTML += "<div class='producto' style='width:185%;'><img src='resources/cta/cta2.jpg'></img></div></br>";
        }
        muestraCatalogo.innerHTML += "<div class='producto' style='width:92.5%;' >" + "<h3>" + productos[y].name + "</h3>" + "<h3>" + productos[y].price + "</h3>" + "<img src='" + productos[y].image + "'></img>" + "<button class='button'>" + productos[y].button_text + "</button></div>";
    }
}

function recarga() {
    //función encontrada en internet y aplicada al array para que salga desordenado
    productos.sort(function() { return Math.random() - 0.5 });
    //llamada función que muestra el catalogo debajo del anterior
    mostrarCatalogo();
}
mostrarCatalogo();